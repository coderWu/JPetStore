package petstore.domain;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by tujie on 2017/5/7.
 */
public class ViewedItem implements Serializable{
    private static final long serialVersionUID = 6804536240033522157L;

    private String username;
    private String itemId;
    private String datetime;
    private String productId;
    private BigDecimal listPrice;
    private String attribute1;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }
}
