package petstore.persistence;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;
import petstore.domain.Category;


@MapperScan
public interface CategoryMapper {

  List<Category> getCategoryList();

  Category getCategory(String categoryId);

}
