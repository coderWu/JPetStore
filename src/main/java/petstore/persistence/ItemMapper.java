package petstore.persistence;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.annotation.MapperScan;
import petstore.domain.Item;
import petstore.domain.ViewedItem;


@MapperScan
public interface ItemMapper {

  void updateInventoryQuantity(Map<String, Object> param);

  int getInventoryQuantity(String itemId);

  List<Item> getItemListByProduct(String productId);

  Item getItem(String itemId);

  List<Item> getItemList();

  void updateQuantityByItemId(Map<String, Object> param);

  List<ViewedItem> getViewedItemList();

  void addViewedItem(Map<String,String> param);

}
