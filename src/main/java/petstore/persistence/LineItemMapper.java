package petstore.persistence;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;
import petstore.domain.LineItem;


@MapperScan
public interface LineItemMapper {

  List<LineItem> getLineItemsByOrderId(int orderId);

  void insertLineItem(LineItem lineItem);

}
