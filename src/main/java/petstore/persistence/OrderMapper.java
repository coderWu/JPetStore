package petstore.persistence;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;
import petstore.domain.Order;

@MapperScan
public interface OrderMapper {

  List<Order> getOrdersByUsername(String username);

  Order getOrder(int orderId);
  
  void insertOrder(Order order);
  
  void insertOrderStatus(Order order);

}
