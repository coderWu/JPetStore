package petstore.persistence;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;
import petstore.domain.Product;

@MapperScan
public interface ProductMapper {

  List<Product> getProductListByCategory(String categoryId);

  Product getProduct(String productId);

  List<Product> searchProductList(String keywords);

}
