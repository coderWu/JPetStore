package petstore.persistence;

import org.mybatis.spring.annotation.MapperScan;
import petstore.domain.Sequence;


@MapperScan
public interface SequenceMapper {

  Sequence getSequence(Sequence sequence);
  void updateSequence(Sequence sequence);
}
