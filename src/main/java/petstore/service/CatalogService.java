package petstore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import petstore.domain.Category;
import petstore.domain.Item;
import petstore.domain.Product;
import petstore.domain.ViewedItem;
import petstore.persistence.CategoryMapper;
import petstore.persistence.ItemMapper;
import petstore.persistence.ProductMapper;

import java.util.List;
import java.util.Map;

/**
 *
 * Created by Jehu on 2017/5/1.
 */
@Service
public class CatalogService {

    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ProductMapper productMapper;

    public Item getItem(String itemId) {
        return itemMapper.getItem(itemId);
    }

    public boolean isItemInStock(String itemId) {
        return itemMapper.getInventoryQuantity(itemId) > 0;
    }

    public List<Product> getProductListByCategory(String categoryId) {
        return productMapper.getProductListByCategory(categoryId);
    }

    public List<Item> getItemListByProduct(String productId) {
        return itemMapper.getItemListByProduct(productId);
    }

    public Product getProduct(String productId) {
        return productMapper.getProduct(productId);
    }

    public List<Product> searchProductList(String keyword) {
        return productMapper.searchProductList("%" + keyword.toLowerCase() + "%");
    }

    public Category getCategory(String categoryId) {
        return categoryMapper.getCategory(categoryId);
    }

    public List<Item> getItemList(){
        return itemMapper.getItemList();
    }

    public void updateQuantityByItemId(Map<String, Object> param){
        itemMapper.updateQuantityByItemId(param);
    }

    public void addViewedItem(Map<String, String> param){
        itemMapper.addViewedItem(param);
    }

    public List<ViewedItem> getViewedItemList(){
        return itemMapper.getViewedItemList();
    }

    public int getInventoryQuantity(String itemId){
        return itemMapper.getInventoryQuantity(itemId);
    }

}
