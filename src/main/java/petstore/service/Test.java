package petstore.service;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Test
 * Created by Jehu on 2017/4/6.
 */
public class Test {

    public static void main(String[] args) {
        Locale locale = Locale.getDefault();
        ResourceBundle resourceBundle = ResourceBundle.getBundle("resource", locale);
        String test = resourceBundle.getString("login.username");

    }
}
