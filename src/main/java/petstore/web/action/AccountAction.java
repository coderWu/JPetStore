package petstore.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.json.annotations.JSON;
import petstore.domain.Account;
import petstore.service.AccountService;
import petstore.web.util.ActionUtils;

import java.util.*;

/**
 * AccountMapper Action
 * Created by Jehu on 2017/3/15.
 */
public class AccountAction extends ActionSupport
        implements ModelDriven<Account>{

    private AccountService accountService;

    private Account account = new Account();

    private String repeatedPassword;

    private String checkCode;

    private String errorToken = "401";

    private Map<String, Object> data;

    public Map getData() {
        return data;
    }

    public String getErrorToken() {
        return errorToken;
    }

    public String getRepeatedPassword() {
        return repeatedPassword;
    }

    public void setRepeatedPassword(String repeatedPassword) {
        this.repeatedPassword = repeatedPassword;
    }

    public String getCheckCode()
    {
        return checkCode;
    }

    public void setCheckCode(String checkCode)
    {
        this.checkCode = checkCode;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }


    /**
     * 获取传到服务器的参数到一个Account对象
     * @return AccountMapper
     */
    @JSON(serialize = false)
    public Account getModel() {
        return account;
    }

    public String signInPage() {
        return "signInPage";
    }

    public String newAccountPage() {
        return "newAccountPage";
    }

    public String myAccountPage() {
        return "myAccountPage";
    }

    public String error() {
        data = new HashMap<>();
        ActionUtils.setReturnData(data, "400", "重复提交无效");
        return SUCCESS;
    }

    /**
     * API
     * 登录操作
     * @return String
     */
    public String login() {
        data = new HashMap<>();
        ActionUtils.setReturnData(data,"400", "error");

        if (!ActionUtils.checkCheckCode(this.checkCode)) {
            ActionUtils.setReturnData(data, "400", "验证码错误");
            return SUCCESS;
        }

        if (this.account.getUsername() == null ||
                "".equals(this.account.getUsername()) ||
                this.account.getPassword() == null ||
                "".equals(this.account.getPassword())) {
            ActionUtils.setReturnData(data, "400", "用户信息不完整");
            return SUCCESS;
        }

        Account result = accountService.getAccount(account.getUsername(), account.getPassword());

        if (result != null) {
            ActionContext.getContext().getSession()
                    .put("name", this.account.getUsername());
            ActionUtils.setReturnData(data, "200", "登陆成功");
        } else {
            ActionUtils.setReturnData(data, "400", "用户名或密码错误");
        }

        return SUCCESS;
    }

    /**
     * API
     * 登出操作
     * @return String
     */
    public String logout() {
        ActionContext.getContext().getSession().remove("name");
        data = new HashMap<>();
        ActionUtils.setReturnData(data, "200", "登出成功");
        return SUCCESS;
    }

    /**
     * API
     * 获取用户数据
     * 从session获取登陆的用户名，根据用户名获取用户数据
     * @return String
     */
    public String getUser() {
        data = new HashMap<>();
        if (!ActionUtils.isLogin()) {
            ActionUtils.setReturnData(data, "400", "你还未登陆");
            return SUCCESS;
        }
        this.account = accountService.getAccount(ActionUtils.getLoginUsername());
        ActionUtils.setReturnData(data, "200", account);
        return SUCCESS;
    }

    /**
     * API
     * 判断用户名是否可用
     * @return String
     */
    public String usefulName() {
        data = new HashMap<>();
        if (this.account.getUsername() == null || "".equals(this.account.getUsername())) {
            ActionUtils.setReturnData(data, "400", "缺少用户名");
            return SUCCESS;
        }
        Account result = accountService.getAccount(this.account.getUsername());
        ActionUtils.setReturnData(data, "200", result == null);
        return SUCCESS;
    }

    /**
     * API
     * 更新用户数据接口
     * @return String
     */
    public String updateUser() {
        data = new HashMap<>();
        if (!ActionUtils.isLogin()) {
            ActionUtils.setReturnData(data, "400", "尚未登陆");
            return SUCCESS;
        }
        if (!ActionUtils.getLoginUsername().equals(account.getUsername())) {
            ActionUtils.setReturnData(data, "400", "登陆用户与操作用户不一致");
            return SUCCESS;
        }
        //TODO 保存用户信息时因数据太多太琐碎暂不处理后台判断是否为空
        if (!account.getPassword().equals(repeatedPassword)) {
            ActionUtils.setReturnData(data, "400", "两次输入密码不一致");
            return SUCCESS;
        }

        accountService.updateAccount(this.account);
        ActionUtils.setReturnData(data, "200", "更新成功");
        return SUCCESS;
    }


    /**
     * API
     * 添加用户
     * @return String
     */
    public String addUser() {
        data = new HashMap<>();
        //TODO 保存用户信息时因数据太多太琐碎暂不处理后台判断是否为空
        if (!account.getPassword().equals(repeatedPassword)) {
            ActionUtils.setReturnData(data, "400", "两次输入密码不一致");
            return SUCCESS;
        }

        Account result = accountService.getAccount(account.getUsername());

        if (result != null) {
            ActionUtils.setReturnData(data, "400", "用户名已存在");
            return SUCCESS;
        }

        accountService.insertAccount(account);
        ActionUtils.setReturnData(data, "200", "注册成功");
        return SUCCESS;
    }



}
