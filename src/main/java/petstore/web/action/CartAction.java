package petstore.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import petstore.domain.Cart;
import petstore.domain.Item;
import petstore.service.CatalogService;
import petstore.web.util.ActionUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Cart action
 * Created by Jehu on 2017/3/22.
 */
public class CartAction extends ActionSupport {

    private CatalogService catalogService;

    private String itemId;

    private String count;

    private static Map<String, Object> data;

    public Map getData() {
        return data;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    public String cart() {
        return SUCCESS;
    }

    public String checkOut() {
        return "checkOut";
    }

    public String addItem() {
        if (!hasCart()) {
            createCart();
        }
        data = new HashMap<>();
        if (itemId == null || itemId.equals("") || count == null || "".equals(count)) {
            ActionUtils.setReturnData(data, "400", "添加失败，数据错误");
            return SUCCESS;
        }
        Cart cart = getCart();
        Item item = catalogService.getItem(itemId);
        if (item == null) {
            ActionUtils.setReturnData(data, "400", "物品不存在");
        } else {
            if (!cart.containsItemId(itemId)) {
                boolean isInStock = catalogService.isItemInStock(itemId);
                cart.addItem(item, isInStock);
            }
            cart.setQuantityByItemId(itemId, Integer.parseInt(count));
        }
        setCart(cart);
        ActionUtils.setReturnData(data, "200", "添加成功");
        return SUCCESS;
    }

    public String removeItem() {
        data = new HashMap<>();
        if (!hasCart()) {
            ActionUtils.setReturnData(data, "400", "购物车不存在");
        }
        if (itemId == null || itemId.equals("")) {
            ActionUtils.setReturnData(data, "400", "删除失败，数据错误");
            return SUCCESS;
        }
        Cart cart = getCart();
        cart.removeItemById(itemId);
        setCart(cart);
        ActionUtils.setReturnData(data, "200", "删除成功");
        return SUCCESS;
    }


    public String getCartData() {
        data = new HashMap<>();
        if (hasCart()) {
            ActionUtils.setReturnData(data, "200", getCart());
        } else {
            ActionUtils.setReturnData(data, "400", "购物车为空");
        }
        return SUCCESS;
    }

    private boolean hasCart() {
        return ActionContext.getContext().getSession().containsKey("cart");
    }

    private Cart getCart() {
        Map map = ActionContext.getContext().getSession();
        return (Cart) map.get("cart");
    }

    private void setCart(Cart cart) {
        ActionContext.getContext().getSession().put("cart", cart);
    }

    private void createCart() {
        Cart cart = new Cart();
        ActionContext.getContext().getSession().put("cart", cart);
    }

}
