package petstore.web.action;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.json.annotations.JSON;
import petstore.domain.*;
import petstore.service.AccountService;
import petstore.service.CatalogService;
import petstore.web.util.ActionUtils;

import java.util.*;

/**
 * @author Jehu
 * Catalog Action
 * Created by Jehu on 2017/3/15.
 */
public class CatalogAction extends ActionSupport {

    private CatalogService catalogService;

    private AccountService accountService;

    private String favcatagory;

    private String category;

    private String product;

    private String item;

    private String searchProducts;

    private Map<String, Object> data;

    private String itemId;

    private int quantity;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @JSON(serialize = false)
    public String getFavcatagory() {
        return favcatagory;
    }

    public void setFavcatagory(String favcatagory) {
        this.favcatagory = favcatagory;
    }

    @JSON(serialize = false)
    public String getCategory() {
        return category;
    }

    public void setCategory(String catagory) {
        this.category = catagory;
    }

    @JSON(serialize = false)
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @JSON(serialize = false)
    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @JSON(serialize = false)
    public String getSearchProducts() {
        return searchProducts;
    }

    public void setSearchProducts(String searchProducts) {
        this.searchProducts = searchProducts;
    }

    public String main() {
        return "main";
    }

    public String category() {
        if (category != null && !category.equals("")) {
            setSession("category", category);
            return null;
        }
        return "category";
    }

    public String item() {
        if (item != null && !item.equals("")) {
            setSession("item", item);
            return null;
        }
        return "item";
    }

    public String product() {
        if (product != null && !product.equals("")) {
            setSession("product", product);
            return null;
        }
        return "product";
    }

    public String searchProducts() {
        if (searchProducts != null && !searchProducts.equals("")) {
            setSession("searchProducts", searchProducts);
            return null;
        }
        return "searchProducts";
    }

    public String editItemQuantity(){
        Account account = accountService.getAccount(ActionUtils.getLoginUsername());
        if (!ActionUtils.isLogin() || !account.getAdmin().equals("1") || account == null) {
            return "main";
        }
        return "editItemQuantity";
    }

    public String viewedItems(){
        Account account = accountService.getAccount(ActionUtils.getLoginUsername());
        if (!ActionUtils.isLogin() || !account.getAdmin().equals("1") || account == null) {
            return "main";
        }
        return "viewedItems";
    }

    public String getCategoryData() {
        data = new HashMap<>();
        String viewCategory = getSession("category");
        if (viewCategory == null || viewCategory.equals("")) {
            ActionUtils.setReturnData(data, "400", "获取category失败");
            return SUCCESS;
        }

        List<Product> allProduct = catalogService.getProductListByCategory(viewCategory);

        if (allProduct.size() > 0) {
            ActionUtils.setReturnData(data, "200", allProduct);
        } else {
            ActionUtils.setReturnData(data, "400", "category不存在");
        }
        return SUCCESS;
    }

    public String getProductData() {
        data = new HashMap<>();
        String viewProduct = getSession("product");
        if (viewProduct == null || viewProduct.equals("")) {
            ActionUtils.setReturnData(data, "400", "获取product失败");
            return SUCCESS;
        }

        List<Item> allItem = catalogService.getItemListByProduct(viewProduct);
        if (allItem.size() > 0) {
            List result = new ArrayList();
            Product product = catalogService.getProduct(viewProduct);
            result.add(0, product);
            result.add(1, allItem);
            ActionUtils.setReturnData(data, "200", result);
        } else {
            ActionUtils.setReturnData(data, "400", "product不存在");
        }
        return SUCCESS;
    }

    public String getItemData() {
        data = new HashMap<>();
        String itemName = getSession("item");
        if (itemName == null || itemName.equals("")) {
            ActionUtils.setReturnData(data, "400", "获取item失败");
            return SUCCESS;
        }
        Item itemDetail = catalogService.getItem(itemName);
        if (itemDetail != null) {
            ActionUtils.setReturnData(data, "200", itemDetail);
        } else {
            ActionUtils.setReturnData(data, "400", "item不存在");
        }
        return SUCCESS;
    }

    public String searchProduct() {
        data = new HashMap<>();
        String searchName = getSession("searchProducts");
        if (searchName == null || searchName.equals("")) {
            ActionUtils.setReturnData(data, "400", "请输入搜索内容");
            return SUCCESS;
        }
        List<Product> products = catalogService.searchProductList(searchName);
        if (products.size() > 0) {
            ActionUtils.setReturnData(data, "200", products);
        } else {
            ActionUtils.setReturnData(data, "400", "查询内容不存在");
        }
        return SUCCESS;
    }

    public String searchRemind() {
        data = new HashMap<>();
        searchProducts = (searchProducts == null || searchProducts.trim().equals("")) ? "_" : searchProducts;
        List<Product> products = catalogService.searchProductList(searchProducts);
        ActionUtils.setReturnData(data, "200", products);
        return SUCCESS;
    }

    public String getFavoriteProduct() {
        data = new HashMap<>();
        if (!ActionUtils.isLogin()) {
            ActionUtils.setReturnData(data, "400", "未登录");
            return SUCCESS;
        }
        Account account = accountService.getAccount(ActionUtils.getLoginUsername());
        List<Product> products = catalogService.getProductListByCategory(account.getUsername());
        ActionUtils.setReturnData(data, "200", products);
        return SUCCESS;
    }

    public String getCatagoryData() {
        data = new HashMap<>();
        if (category == null || "".equals(category)) {
            ActionUtils.setReturnData(data, "400", "获取失败");
            return SUCCESS;
        }
        List<Product> productsDate = catalogService.getProductListByCategory(category);
        Category categoryData = catalogService.getCategory(category);
        List result = new ArrayList();
        result.add(0, categoryData);
        result.add(1, productsDate);
        ActionUtils.setReturnData(data, "200", result);
        return SUCCESS;
    }


    private String getSession(String type) {
        return (String) ActionContext.getContext().getSession().get(type);
    }

    private void setSession(String name, String value) {
        ActionContext.getContext().getSession().put(name, value);
    }

    public String getItemList(){
        data = new HashMap<>();
        Account account = accountService.getAccount(ActionUtils.getLoginUsername());
        if (!ActionUtils.isLogin() || !account.getAdmin().equals("1") || account == null) {
            ActionUtils.setReturnData(data, "400", "非管理员操作");
            return SUCCESS;
        }
        List<Item> itemList = catalogService.getItemList();
        ActionUtils.setReturnData(data, "200", itemList);
        return SUCCESS;
    }

    public String updateItemQuantity(){
        data = new HashMap<>();
        Account account = accountService.getAccount(ActionUtils.getLoginUsername());
        if (!ActionUtils.isLogin() || !account.getAdmin().equals("1") || account == null) {
            ActionUtils.setReturnData(data, "400", "非管理员操作");
            return SUCCESS;
        }
        Map<String, Object> param = new  HashMap<>();
        param.put("itemId", itemId);
        param.put("quantity", quantity);
        catalogService.updateQuantityByItemId(param);
        ActionUtils.setReturnData(data, "200", null);
        return SUCCESS;
    }



    public String getViewedItemList(){
        data = new HashMap<>();
        Account account = accountService.getAccount(ActionUtils.getLoginUsername());
        if (!ActionUtils.isLogin() || !account.getAdmin().equals("1") || account == null) {
            ActionUtils.setReturnData(data, "400", "非管理员操作");
            return SUCCESS;
        }
        List<ViewedItem> viewedItemList = catalogService.getViewedItemList();
        ActionUtils.setReturnData(data, "200", viewedItemList);
        return SUCCESS;
    }

    public String isStockEnough(){
        data = new HashMap<>();
        int num = catalogService.getInventoryQuantity(itemId);
        boolean isEnough = num - quantity >= 0;
        ActionUtils.setReturnData(data, "200", isEnough);
        return SUCCESS;
    }
}
