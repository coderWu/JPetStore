package petstore.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Locale;

/**
 * Created by zhangzhenghao on 17/4/23.
 */
public class ChangelanAction  extends ActionSupport {
    public String execute() throws Exception {
        Locale locale=Locale.getDefault();
        ActionContext.getContext().getSession().put("WW-TRANS-I18N-LOCALE", locale);
        return SUCCESS;
    }
}
