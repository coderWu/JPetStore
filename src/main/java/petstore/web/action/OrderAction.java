package petstore.web.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.json.annotations.JSON;
import petstore.domain.Account;
import petstore.domain.Cart;
import petstore.domain.Order;
import petstore.service.AccountService;
import petstore.service.OrderService;
import petstore.web.util.ActionUtils;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Order Action
 * Created by Jehu on 2017/3/24.
 */
public class OrderAction extends ActionSupport
        implements ModelDriven<Order>{

    private OrderService orderService;

    private AccountService accountService;

    private Map<String, Object> data;

    private Order order = new Order();

    private boolean different = true;

    private boolean confirm = false;

    private String viewOrderId;

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public boolean isDifferent() {
        return different;
    }

    public void setDifferent(boolean different) {
        this.different = different;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public void setViewOrderId(String viewOrderId) {
        this.viewOrderId = viewOrderId;
    }

    public String getViewOrderId() {
        return this.viewOrderId;
    }

    public Map<String, Object> getData() {
        return data;
    }

    @Override
    @JSON (serialize = false)
    public Order getModel() {
        return order;
    }

    public String newOrder() {
        Order order = new Order();
        Account account = accountService.getAccount(ActionUtils.getLoginUsername());
        Cart cart = ActionUtils.getCart();
        if (cart != null) {
            order.initOrder(account, ActionUtils.getCart());
        } else {
            order.initOrder(account, new Cart());
        }
        setSessionOrder(order);
        return "newOrder";
    }

    public String shipForm() {
        return "shipForm";
    }

    public String confirmOrder() {
        return "confirmOrder";
    }

    public String viewOrder() {
        if (viewOrderId != null && !viewOrderId.equals("")) {
            setSessionViewOrderId(Integer.parseInt(viewOrderId));
            return null;
        }
        return "viewOrder";
    }

    public String listOrders() {
        return "listOrders";
    }

    public String getOrder() {
        Order order = getSessionOrder();
        data = new HashMap<>();
        if (order == null) {
            ActionUtils.setReturnData(data, "400", "订单不存在");
            return SUCCESS;
        }
        ActionUtils.setReturnData(data, "200", order);
        return SUCCESS;
    }

    public String editOrder() {
        Order sessionOrder = getSessionOrder();
        data = new HashMap<>();
        if (sessionOrder == null) {
            ActionUtils.setReturnData(data, "400", "不存在订单");
            return SUCCESS;
        }
        sessionOrder.setBillAddress1(order.getBillAddress1());
        sessionOrder.setBillAddress2(order.getBillAddress2());
        sessionOrder.setBillCity(order.getBillCity());
        sessionOrder.setBillState(order.getBillState());
        sessionOrder.setBillZip(order.getBillZip());
        sessionOrder.setBillCountry(order.getBillCountry());
        sessionOrder.setBillToFirstName(order.getBillToFirstName());
        sessionOrder.setBillToLastName(order.getBillToLastName());
        sessionOrder.setCardType(order.getCardType());
        sessionOrder.setExpiryDate(order.getExpiryDate());
        sessionOrder.setCreditCard(order.getCreditCard());
        if (!this.order.isDifferent()) {
            sessionOrder.setShipAddress1(order.getBillAddress1());
            sessionOrder.setShipAddress2(order.getBillAddress2());
            sessionOrder.setShipCity(order.getBillCity());
            sessionOrder.setShipState(order.getBillState());
            sessionOrder.setShipZip(order.getBillZip());
            sessionOrder.setShipCountry(order.getBillCountry());
            sessionOrder.setShipToFirstName(order.getBillToFirstName());
            sessionOrder.setShipToLastName(order.getBillToLastName());
        }
        setSessionOrder(sessionOrder);
        ActionUtils.setReturnData(data, "200", "");
        return SUCCESS;
    }

    public String editShip() {
        Order sessionOrder = getSessionOrder();
        data = new HashMap<>();
        if (sessionOrder == null) {
            ActionUtils.setReturnData(data, "400", "不存在订单");
            return SUCCESS;
        }
        sessionOrder.setShipAddress1(order.getShipAddress1());
        sessionOrder.setShipAddress2(order.getShipAddress2());
        sessionOrder.setShipCity(order.getShipCity());
        sessionOrder.setShipState(order.getShipState());
        sessionOrder.setShipZip(order.getShipZip());
        sessionOrder.setShipCountry(order.getShipCountry());
        sessionOrder.setShipToFirstName(order.getShipToFirstName());
        sessionOrder.setShipToLastName(order.getShipToLastName());
        setSessionOrder(sessionOrder);
        ActionUtils.setReturnData(data, "200", "");
        return SUCCESS;
    }

    public String confirm() {
        data = new HashMap<>();
        ActionUtils.setReturnData(data, "400", "提交出错");
        Order order = getSessionOrder();
        if (this.confirm && order != null) {

            int orderId = orderService.insertOrder(order);
            setSessionViewOrderId(orderId);
            ActionUtils.setCart(new Cart());
            setSessionOrder(null);
            ActionUtils.setReturnData(data, "200", "提交成功");
        }
        return SUCCESS;
    }

    public String getViewOrder() {
        try {
            Order order = orderService.getOrder(getSessionViewOrderId());
            data = new HashMap<>();
            if (order == null) {
                ActionUtils.setReturnData(data, "400", "订单不存在");
                return SUCCESS;
            }
            ActionUtils.setReturnData(data, "200", order);
            return SUCCESS;
        } catch (Exception e) {
            ActionUtils.setReturnData(data, "400", "订单不存在");
            return SUCCESS;
        }
    }

    public String getMyOrders() {
        data = new HashMap<>();
        if (!ActionUtils.isLogin()) {
            ActionUtils.setReturnData(data, "400", "还未登录");
            return SUCCESS;
        }
        Account account = accountService.getAccount(ActionUtils.getLoginUsername());
        List<Order> orders = orderService.getOrdersByUsername(account.getUsername());
        ActionUtils.setReturnData(data, "200", orders);
        return SUCCESS;
    }

    private Order getSessionOrder() {
        return (Order) ActionContext.getContext().getSession().get("order");
    }

    private void setSessionOrder(Order order) {
        ActionContext.getContext().getSession().put("order", order);
    }

    private int getSessionViewOrderId() {
        return (int) ActionContext.getContext().getSession().get("orderId");
    }

    private void setSessionViewOrderId(int id) {
        ActionContext.getContext().getSession().put("orderId", id);
    }

}
