package petstore.web.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import org.apache.struts2.ServletActionContext;


/**
 * 拦截GET请求
 * Created by Jehu on 2017/3/16.
 */
public class GetInterceptor extends AbstractInterceptor {
    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        String method = ServletActionContext.getRequest().getMethod();
        if(method.equalsIgnoreCase("POST")){
            return actionInvocation.invoke();
        }
        return "json";
    }
}
