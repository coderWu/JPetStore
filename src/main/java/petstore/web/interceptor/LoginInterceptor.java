package petstore.web.interceptor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import java.util.Map;

/**
 * 检测是否登陆的拦截器
 * Created by Jehu on 2017/3/15.
 */
public class LoginInterceptor extends AbstractInterceptor {

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        // 取得请求相关的ActionContext实例
        ActionContext ctx = invocation.getInvocationContext();
        Map session = ctx.getSession();
        //取出名为name的session属性
        String user = (String) session.get("name");
        if (user == null || user.equals("")) {
            //没有登陆，将服务器提示设置成一个HttpServletRequest属性
            return "login";
        }
        return invocation.invoke();
    }


}
