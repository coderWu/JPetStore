package petstore.web.interceptor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import petstore.service.CatalogService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tujie on 2017/4/30.
 */
public class ViewedItemInterceptor extends AbstractInterceptor {
    private CatalogService catalogService;

    public CatalogService getCatalogService() {
        return catalogService;
    }

    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    private String username = null;
    private String itemId = null;
    private String paramName = null;

    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        ActionContext actionContext = actionInvocation.getInvocationContext();
        Map session = actionContext.getSession();
        Map parameters = actionContext.getParameters();
        if (session.get("name") == null){
            return actionInvocation.invoke();
        }else {
            username = (String)session.get("name");
        }
        String classname = actionContext.getName();
        if ("CatalogPage_item".equals(classname) || classname == "CatalogPage_item"){
            paramName = "item";
        }else {
            return actionInvocation.invoke();
        }

        if (parameters.get("item") == null || parameters.size() == 0){
            return actionInvocation.invoke();
        }else {
            itemId = parameters.get(paramName).toString();
        }

        Map<String, String> param = new HashMap<>();
        param.put("username", username);
        param.put("itemId", itemId);

        catalogService.addViewedItem(param);

        return actionInvocation.invoke();
    }
}
