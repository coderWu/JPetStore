package petstore.web.util;

import com.opensymphony.xwork2.ActionContext;
import petstore.domain.Cart;
import java.util.*;

/**
 * 公共函数
 * Created by Jehu on 2017/3/16.
 */
public class ActionUtils {

    /**
     * 根据session判断是否已经登陆
     * @return boolean
     */
    public static boolean isLogin() {
        String username = (String) ActionContext.getContext().getSession().get("name");
        return username != null && !username.equals("");
    }

    /**
     * 根据session获取已经登陆的用户名
     * @return String
     */
    public static String getLoginUsername() {
        return (String) ActionContext.getContext().getSession().get("name");
    }

    public static void setReturnData (Map<String, Object> map, String code, Object obj) {
        map.put("code", code);
        map.put("data", obj);
    }

    public static boolean checkCheckCode(String checkCode) {
        if (checkCode == null || "".equals(checkCode)) {
            return false;
        }
        String sessionCheckCode = (String)ActionContext.getContext().getSession().get("checkCode");
        return checkCode.equals(sessionCheckCode);
    }

    public static Cart getCart() {
        return (Cart) ActionContext.getContext().getSession().get("cart");
    }

    public static void setCart(Cart cart) {
        ActionContext.getContext().getSession().put("cart", cart);
    }
}
