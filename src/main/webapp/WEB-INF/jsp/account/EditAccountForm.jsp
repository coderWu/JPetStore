<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/15
  Time: 23:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>

<div id="Catalog">


    <div id="user-form">
        <h3><s:text name="for.userinfo"></s:text></h3>
        <table>
            <tbody>
            <tr>
                <td><s:text name="for.Username"></s:text></td>
                <td id="user-id"></td>
            </tr>
            <tr>
                <td><s:text name="for.Password"></s:text></td>
                <td><input id="password" name="password"
                           type="password"></td>
            </tr>
            <tr>
                <td><s:text name="for.repeat"></s:text></td>
                <td><input id="repeat-password" name="repeatedPassword" type="password"></td>
            </tr>
            </tbody>
        </table>
        <%@ include file="IncludeAccountFields.jsp" %>
        <br><input name="editAccount" value="<s:text name="for.saveaccount"/>" id="save-account"
               type="submit"> <br>
        <span><br><a style="display: none;color: red;" id="error-message"></a></span>
    </div>
    <a href="Order_listOrders"><s:text name="for.myorder"></s:text></a>

</div>
<script type="application/javascript">
    var error_message = $("#error-message");
    var username;
    $("#save-account").click(function () {
        error_message.hide();
        if (username == "") {
            error_message.text("请登录后操作").show();
        }else if (checkFormValue() == 0) {
            saveUser();
        }
    });

    $(document).ready(function () {
        getUser();
    });

    function checkFormValue() {
        var noNone = 0;
        $("#user-form input").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            if (e_name != "repeatedPassword" && e_name != "password") {
                if (value == "") {
                    noNone++;
                    removeErrorMessage($(this));
                    $(this).after(showErrorMessage("请填写该项"));
                } else {
                    //如果后面有之前残留的错误提示则清空
                    removeErrorMessage($(this));
                }
            }
        });
        if ($("#repeat-password").val() != $("#password").val()) {
            removeErrorMessage($("#repeat-password"));
            $("#repeat-password").after(showErrorMessage("两次密码不一致"));
            noNone += 2;
        }
        return noNone;
    }

    function saveUser() {
        console.log(getUserInfo());
        getUserInfo().forEach(function (p1, p2, p3) {
            console.log(p1);
            console.log(p2);
            console.log(p3);
        })
        console.log(getUserInfo().getAll(1));
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/_Account_updateUser",
            data : getUserInfo(),
            type : "POST",
            dataType : "json",
            cache: false,
            processData: false,
            contentType: false,
            success : function (data) {
                 if (data.code == 200) {
                     if (data.data == "更新成功") {
                         error_message.css("color", "green");
                     } else {
                         error_message.css("color", "red");
                     }
                     error_message.text(data.data).show();
                 } else {
                     error_message.text(data.data).show();
                 }
            },
            error : function () {
                error_message.text("网络连接失败");
            }
        });
    }

    function getUserInfo() {
        var formData = new FormData();
        $("#user-form input").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            if (e_name != "editAccount") {
                if($(this).is(':checked')) {
                    formData.append(e_name, true);
                } else {
                    formData.append(e_name, value);
                }
            }
        });
        $("#user-form select").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            formData.append(e_name, value);
        });
        formData.append("username", username);
        return formData;
    }

    function getUser() {
        if (loginUser == 400) {
            error_message.text("网络连接失败");
            return;
        }
        if (loginUser.code == 200) {
            var userinfo = loginUser.data;
            username = userinfo.username;
            $("#user-id").text(userinfo.username);
            $("#user-firstname").val(userinfo.firstName);
            $("#user-lastname").val(userinfo.lastName);
            $("#user-email").val(userinfo.email);
            $("#user-phone").val(userinfo.phone);
            $("#user-address1").val(userinfo.address1);
            $("#user-address2").val(userinfo.address2);
            $("#user-city").val(userinfo.city);
            $("#user-state").val(userinfo.state);
            $("#user-zip").val(userinfo.zip);
            $("#user-country").val(userinfo.country);
            $("#user-prolan").val(userinfo.languagePreference);
            $("#user-favcate").val(userinfo.favouriteCategoryId);
            $("#user-banneropt").attr("checked", userinfo.bannerOption);
            $("#user-listopt").attr("checked", userinfo.listOption);
        } else {
            username = "";
            if (loginUser.data == "你还未登陆") {
                $("#user-id").text(loginUser.data);
            } else {
                error_message.text(loginUser.data);
            }
        }
    }

</script>

<%@ include file="../common/IncludeBottom.jsp" %>
