<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/13
  Time: 21:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<h3><s:text name="for.accountinfo"></s:text></h3>

<table>
    <tbody>
    <tr>
        <td><s:text name="for.firstname"></s:text></td>
        <td><input name="firstName" value="" type="text" id="user-firstname"></td>
    </tr>
    <tr>
        <td><s:text name="for.lastname"></s:text></td>
        <td><input name="lastName" value="" type="text" id="user-lastname"></td>
    </tr>
    <tr>
        <td><s:text name="for.email"></s:text></td>
        <td><input name="email"
                   value="" type="text" size="40" id="user-email"></td>
    </tr>
    <tr>
        <td><s:text name="for.phone"></s:text></td>
        <td><input name="phone" value=""
                   type="text" id="user-phone"></td>
    </tr>
    <tr>
        <td><s:text name="for.address1"></s:text></td>
        <td><input name="address1"
                   value="" type="text" size="40" id="user-address1"></td>
    </tr>
    <tr>
        <td><s:text name="for.address2"></s:text></td>
        <td><input name="address2" value=""
                   type="text" size="40" id="user-address2"></td>
    </tr>
    <tr>
        <td><s:text name="for.city"></s:text></td>
        <td><input name="city" value="" type="text" id="user-city"></td>
    </tr>
    <tr>
        <td><s:text name="for.state"></s:text></td>
        <td><input name="state" value="" type="text"
                   size="4" id="user-state"></td>
    </tr>
    <tr>
        <td><s:text name="for.zip"></s:text></td>
        <td><input name="zip" value="" type="text"
                   size="10" id="user-zip"></td>
    </tr>
    <tr>
        <td><s:text name="for.country"></s:text></td>
        <td><input name="country" value="" type="text"
                   size="15" id="user-country"></td>
    </tr>
    </tbody>
</table>

<h3><s:text name="for.profileinfo"></s:text></h3>

<table>
    <tbody>
    <tr>
        <td><s:text name="for.languagepreference"></s:text></td>
        <td><select name="languagePreference" id="user-prolan">
            <option value="english" id="english">english</option>
            <option value="japanese" id="japanese">japanese</option>
        </select></td>
    </tr>
    <tr>
        <td><s:text name="for.favorite"></s:text></td>
        <td><select name="favouriteCategoryId" id="user-favcate">
            <option value="FISH" id="FISH">FISH</option>
            <option value="DOGS" id="DOGS">DOGS</option>
            <option value="REPTILES" id="REPTILES">REPTILES</option>
            <option value="CATS" id="CATS">CATS</option>
            <option value="BIRDS" id="BIRDS">BIRDS</option>
        </select></td>
    </tr>
    <tr>
        <td><s:text name="for.enableMylist"></s:text></td>
        <td><input name="listOption" value="false"
                   type="checkbox" id="user-listopt"></td>
    </tr>
    <tr>
        <td><s:text name="for.enableMyBanner"></s:text></td>
        <td><input name="bannerOption" value="false"
                   type="checkbox"  id="user-banneropt"></td>
    </tr>

    </tbody>
</table>

