<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/13
  Time: 21:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>
<script type="application/javascript">
    var error_message = $("#error-message");
    function usernameIsExist() {
        var username = $("#username").val();
        var usernameInfo = $("#usernameInfo");
        if (username != "") {
            $.ajax({
                url : "//127.0.0.1:8080/JPetStore/_Account_usefulName",
                type : "POST",
                data : {username : username},
                dataType : "json",
                success : function (data) {
                    if (data.code == 200) {
                        if (data.data) {
                            usernameInfo.empty();
                            usernameInfo.append("<span style='color:green'>The User ID is Available</span>");
                        } else {
                            usernameInfo.empty();
                            usernameInfo.append("<span style='color:red'>The User ID Is Exist</span>");
                        }
                    } else {
                        usernameInfo.empty();
                        usernameInfo.append("<span style='color:red'>" + data.data + "</span>")
                    }
                },
                error : function () {
                    usernameInfo.empty();
                    usernameInfo.append("<p color='red'>Can't Check Is The User ID Exist</p>")
                }
            });
        }
    }

</script>
<div id="Catalog">

    <div id="new-user-form">

        <h3><s:text name="for.userinfo"></s:text></h3>

        <table>
            <tr>
                <td><s:text name="for.Username"></s:text></td>
                <td>
                    <input type="text" name="username" id="username" onblur="usernameIsExist();"/>
                    <div id="usernameInfo"></div>
                </td>
            </tr>
            <tr>
                <td><s:text name="for.Password"></s:text></td>
                <td><input type="password" name="password" id="password"/></td>
            </tr>
            <tr>
                <td><s:text name="for.repeat"></s:text></td>
                <td><input type="password" name="repeatedPassword" id="repeat-password"/></td>
            </tr>
        </table>

        <%@ include file="IncludeAccountFields.jsp" %>

        <br><input type="submit" id="add-account" name="newAccount" value="<s:text name="for.saveaccount"/>"/>
        <br><span><br><a style="display: none;color: red;" id="error-message"></a></span>
    </div>
</div>

<script type="text/javascript">
    var error_message = $("#error-message");
    function checkFormValue() {
        var noNone = 0;
        $("#new-user-form input").each(function () {
            var value = $(this).val();
            if (value == "") {
                noNone++;
                removeErrorMessage($(this));
                $(this).after(showErrorMessage("请填写该项"));
            } else {
                //如果后面有之前残留的错误提示则清空
                removeErrorMessage($(this));
            }
        });
        if ($("#repeat-password").val() != $("#password").val()) {
            removeErrorMessage($("#repeat-password"));
            $("#repeat-password").after(showErrorMessage("两次密码不一致"));
        }
        return noNone;
    }

    function getUserInfo() {
        var formData = new FormData();
        $("#new-user-form input").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            if (e_name != "newAccount") {
                if($(this).is(':checked')) {
                    formData.append(e_name, true);
                } else {
                    formData.append(e_name, value);
                }
            }
        });
        $("#new-user-form select").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            formData.append(e_name, value);
        });
        return formData;
    }

    function addUser() {
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/_Account_addUser",
            data : getUserInfo(),
            type : "POST",
            dataType : "json",
            cache: false,
            processData: false,
            contentType: false,
            success : function (data) {
                if (data.code == 200) {
                    if (data.data == "注册成功") {
                        error_message.css("color", "green");
                    } {
                        error_message.css("color", "green");
                    }
                    error_message.text(data.data).show();
                } else {
                    error_message.text(data.data).show();
                }
            },
            error : function () {
                error_message.text("网络连接失败");
            }
        });
    }

    $("#add-account").click(function () {
        error_message.hide();
        if (checkFormValue() == 0) {
            addUser();
        }
    });
</script>

<%@ include file="../common/IncludeBottom.jsp" %>