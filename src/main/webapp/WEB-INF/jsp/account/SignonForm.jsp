<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/13
  Time: 21:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>

<div id="Catalog">
        <%--<form>--%>
        <%--<s:form action="loginValidateAction" theme="simple">--%>
            <s:token></s:token>
            <p><s:text name="for.please"></s:text></p>
            </p>
            <p>
            <table>
                <tbody>
                <tr>
                    <td><s:text   name="for.Username"></s:text></td>
                    <td><input type="text" name="username" id="username"/></td>
                </tr>
                <tr>
                    <td><s:text name="for.Password"></s:text></td>
                    <td><input type="password" name="password" id="password"/></td>
                </tr>
                <tr>
                    <td><s:text name="for.Validate"></s:text></td>
                    <td><input type="text" name="checkCode" id="checkCode"/></td>
                </tr>
                　  </tbody>
                　</table>　
            　　　　　<!--若要点击图片刷新，重新得到一个验证码，要在后面加上个随机数，这样保证每次提交过去的都是不一样的path，防止因为缓存而使图片不刷新-->
            <img src="createImageAction" onclick="this.src='createImageAction?'+ Math.random()" title="点击图片刷新验证码"/>
            <br/>
            <p style="display: none;color: red" id="error-message">error_message</p>
            <br>
            <input type="submit" name="signon" id="login" value="<s:text name="for.login" />"/><br/>


        <%--</s:form>--%>
        <%--</form>--%>

        <s:text name="for.neednew"></s:text><a href="AccountPage_newAccountPage"><s:text name="for.register"></s:text></a>

    </div>
<%@ include file="../common/IncludeBottom.jsp" %>
<script>
    function reloadImage() {
        //document.getElementById("btn").disabled=true;
        document.getElementById("imgservlet").src = 'validation';
    }

    $(document).ready(function () {
        var error = $("#error-message");
        $("#login").click(function () {
            var _token = $('[name="token"]').val().trim();
            var token_name = $('[name="struts.token.name"]').val().trim();
            var name = $("#username").val().trim();
            var pwd = $("#password").val().trim();
            var checkCode = $("#checkCode").val().trim();
            if (name == "") {
                error.text("Please input name").show();
                return;
            }
            if (pwd == "") {
                error.text("Please input password").show();
                return;
            }
            if (checkCode == "") {
                error.text("Please input Validate").show();
                return;
            }
            $.ajax({
                url : "http://127.0.0.1:8080/JPetStore/token_Account_login",
                type : "POST",
                data : {
                    username : name,
                    password : pwd,
                    checkCode : checkCode,
                    'struts.token.name' : token_name,
                    token : _token
                },
                dataType : "json",
                success : function (data) {
                    if (data == 401) {
                        error.text("token失效，请刷新页面后操作");
                    }
                    if (data.code == 200) {
                        window.location.href="//127.0.0.1:8080/JPetStore/CatalogPage_main";
                    } else {
                        error.text(data.data).show();
                    }
                },
                error : function (data) {
                    error.text("error");
                }
            });
        });
    })
</script>
