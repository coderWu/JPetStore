<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/19
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>
<div id="BackLink">
    <a href="CatalogPage_main"><s:text name="for.returntomain"></s:text></a>
</div>

<div id="Catalog">

    <div id="Cart">

        <h2>Shopping Cart</h2>
        <%--<form action="" method="post">--%>
            <table id="cart-list">
                <tr>
                    <th><b><s:text name="for.itemid"></s:text></b></th>
                    <th><b><s:text name="for.proid"></s:text></b></th>
                    <th><b><s:text name="for.des"></s:text></b></th>
                    <th><b><s:text name="for.inshock"></s:text></b></th>
                    <th><b><s:text name="for.Quantity"></s:text></b></th>
                    <th><b><s:text name="for.price"></s:text></b></th>
                    <th><b><s:text name="for.totalcost"></s:text></b></th>
                    <th>&nbsp;</th>
                </tr>
                <!-- 判断 -->
                <tr id="cart-message" style="display: none">
                    <td colspan="8"><b>Your cart is empty.</b></td>
                </tr>
                <!-- 循环 -->
            </table>
        <%--</form>--%>

        <!-- 需要判断是否登录 -->
        <a href="CartPage_checkOut" class="Button" id="check-out-button"><s:text name="for.checkout"></s:text></a>
    </div>

    <div id="MyList"  style="display: none">
        <!-- 判断 -->
        <%@ include file="IncludeMyList.jsp"%>
    </div>

    <div id="Separator">&nbsp;</div>
</div>

<script type="application/javascript">
    $(document).ready(function () {
        getCart(false);
    });

    function updateCartAction() {
        $(".cart-item").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            updateCart(e_name, value);
        });
        location.reload();
    }

    function removeItem(itemId) {
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/_Cart_removeItem",
            type : "POST",
            data : {itemId : itemId},
            dataType : "json",
            success : function (data) {
                if (data.code == 200) {
                    $("#" + itemId).remove();
                }
            }
        });
    }



</script>

<%@ include file="../common/IncludeBottom.jsp" %>

