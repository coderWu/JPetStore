<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/19
  Time: 14:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>

<div id="BackLink">
    <a href="CartPage_cart"><s:text name="for.Returntoshopcart"></s:text></a>
</div>

<div id="Catalog">

    <table>
        <tr>
            <td>
                <h2><s:text name="for.checksummary"></s:text></h2>

                <table id="cart-list">

                    <tr>
                        <td><b><s:text name="for.itemid"></s:text></b></td>
                        <td><b><s:text name="for.proid"></s:text></b></td>
                        <td><b><s:text name="for.des"></s:text></b></td>
                        <td><b><s:text name="for.inshock"></s:text></b></td>
                        <td><b><s:text name="for.Quantity"></s:text></b></td>
                        <td><b><s:text name="for.price"></s:text></b></td>
                        <td><b><s:text name="for.totalcost"></s:text></b></td>
                    </tr>
                    <tr id="cart-message" style="display: none">
                        <td colspan="8"><b>Your cart is empty.</b></td>
                    </tr>
                    <!-- 循环 -->
                </table>

        </tr>
    </table>
    <input value="<s:text name="for.continue"/>" type="submit" id="check-out">
</div>
<script type="application/javascript">
    $(document).ready(function () {
        getCart(true);
    });
    $("#check-out").click(function () {
        window.location.href = "//127.0.0.1:8080/JPetStore/Order_newOrder";
    });
</script>



<%@ include file="../common/IncludeBottom.jsp" %>
