<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/19
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<p>
    Pet Favorites <br /> Shop for more of your favorite pets here.
</p>
<ul id="favorite-list">
    <!-- 循环 -->
</ul>
<script type="application/javascript">

    $(document).ready(function () {
       $.ajax({
           url : "//127.0.0.1:8080/JPetStore/_Catalog_getFavoriteProduct",
           type : "POST",
           success : function (data) {
               if (data.code == 200) {
                   $.each(data.data, function (k, v) {
                       $("#favorite-list").append(
                           "<li><a onclick='javascript:viewProduct(" + '"' +  v.productid +
                           '"' + ")' href='javascript:void(0)'>" + v.name + "</a>" + v.productid + "</li>"
                       );
                       $("#MyList").show();
                   });
               }
           }
       });
    });

</script>

