<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/17
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>

<div id="BackLink">
    <a href="CatalogPage_main"><s:text name="for.returntomain"></s:text></a>
</div>

<div id="Catalog">

    <h2 id="category-name">category name</h2>

    <table id="product-list">
        <tr>
            <th><s:text name="for.proid"></s:text></th>
            <th><s:text name="for.proname"></s:text></th>
        </tr>
        <!-- 循环 -->
    </table>

</div>

<script>
$(document).ready(function () {
    getProduct();
});
</script>

<%@ include file="../common/IncludeBottom.jsp" %>
