<%--
  Created by IntelliJ IDEA.
  User: tujie
  Date: 2017/5/6
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>
<div id="BackLink">
    <a href="CatalogPage_main"><s:text name="for.returntomain"></s:text></a>
</div>

<div id="Catalog">

    <div id="Cart">

        <h2>Shopping Cart</h2>
        <%--<form action="" method="post">--%>
        <table id="item-list">
            <tr>
                <th><b><s:text name="for.itemid"></s:text></b></th>
                <th><b><s:text name="for.proid"></s:text></b></th>
                <th><b><s:text name="for.des"></s:text></b></th>
                <th><b><s:text name="for.Quantity"></s:text></b></th>
                <th><b><s:text name="for.price"></s:text></b></th>
                <th><b>&nbsp;</b></th>
            </tr>
            <%--<!-- 判断 -->--%>
            <%--<tr id="cart-message" style="display: none">--%>
                <%--<td colspan="8"><b>Your cart is empty.</b></td>--%>
            <%--</tr>--%>
            <%--<!-- 循环 -->--%>
        </table>


    </div>

    <%--<div id="MyList"  style="display: none">--%>
        <%--<!-- 判断 -->--%>
        <%--&lt;%&ndash;<%@ include file="IncludeMyList.jsp"%>&ndash;%&gt;--%>
    <%--</div>--%>

    <div id="Separator">&nbsp;</div>
</div>

<script type="application/javascript">
    $(document).ready(function (){
        getItemList();
    });
</script>

<%@ include file="../common/IncludeBottom.jsp" %>
