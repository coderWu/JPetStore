<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/17
  Time: 18:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>


<div id="BackLink">
    <a id="return-to-product" onclick="javascript:viewProduct('FL-DLH-02')" href="javascript:void(0)">Return
        to Product</a>
</div>

<div id="Catalog">

    <table id="item-detail">

    </table>

</div>
<script>
    $(document).ready(function () {
        getItemDetail();
    })
</script>

<%@ include file="../common/IncludeBottom.jsp" %>
