<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/13
  Time: 17:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>


    <div id="Welcome">
        <div id="WelcomeContent">
           <s:text name="for.main"></s:text>
        </div>
    </div>

    <div id="Main">
        <div id="Sidebar">
            <div id="SidebarContent">
                <a onclick="javascript:viewCategory('FISH')" href="javascript:void(0)"><s:text name="for.image_fish"></s:text></a>
                <br/><s:text name="for.main1"></s:text><br/>
                <a onclick="javascript:viewCategory('DOGS')" href="javascript:void(0)"><s:text name="for.image_dog"></s:text></a>
                <br /><s:text name="for.main2"></s:text><br />
                <a onclick="javascript:viewCategory('CATS')" href="javascript:void(0)"><s:text name="for.image_cat"></s:text></a>
                <br /><s:text name="for.main3"></s:text><br />
                <a onclick="javascript:viewCategory('REPTILES')" href="javascript:void(0)"><s:text name="for.image_reptiles"></s:text></a>
                <br /><s:text name="for.main4"></s:text><br />
                <a onclick="javascript:viewCategory('BIRDS')" href="javascript:void(0)"><s:text name="for.image_bird"></s:text></a>
                <br /><s:text name="for.main5"></s:text>
            </div>
        </div>

        <div id="MainImage">
            <div id="MainImageContent">
                <map name="estoremap">
                    <area title="" alt="BIRDS" coords="72,2,280,250" onclick="javascript:viewCategory('BIRDS')" href="javascript:void(0)" shape="rect" class="catagory" />
                    <area title="" alt="FISH" coords="2,180,72,250" onclick="javascript:viewCategory('FISH')" href="javascript:void(0)" shape="rect" class="catagory" />
                    <area title="" alt="DOGS" coords="60,250,130,320" onclick="javascript:viewCategory('DOGS')" href="javascript:void(0)" shape="rect" class="catagory" />
                    <area title="" alt="REPTILES" coords="140,270,210,340" onclick="javascript:viewCategory('REPTILES')" href="javascript:void(0)" shape="rect" class="catagory" />
                    <area title="" alt="CATS" coords="225,240,295,310" onclick="javascript:viewCategory('CATS')" href="javascript:void(0)" shape="rect" class="catagory" />
                    <area title="" alt="BIRDS" coords="280,180,350,250" onclick="javascript:viewCategory('BIRDS')" href="javascript:void(0)" shape="rect" class="catagory" />
                </map>
                <img height="355" src="images/splash.gif" align="middle" usemap="#estoremap" width="350" />
            </div>
        </div>
        <div id="Separator">&nbsp;</div>
    </div>
<script type="application/javascript">
    $(".catagory").hover(function () {

    });
    $(function() {
        $( document ).tooltip({
            track : true,
            items: "[title]",
            content: function() {
                var result;
                $.ajax({
                    url : "//127.0.0.1:8080/JPetStore/_Catalog_getCatagoryData",
                    type : "POST",
                    async : false,
                    data : {category : $(this).attr("alt")},
                    success : function (data) {
                        result = data;
                    }
                });
                console.log(result);
                var content = result.data[0].description.replace(/\.\.\// ,'') + "<br>";
                $.each(result.data[1], function (k, v) {
                    content += v.name + "<br>";
                    content += v.description.replace(/\.\.\// ,'')+ "<br>";
                });
                //console.log(content);
                return content;
            }
        });
    });
</script>

<%@ include file="../common/IncludeBottom.jsp" %>

