<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/17
  Time: 18:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>

<div id="BackLink">
    <a id="return-to-category" onclick="javascript:viewCategory('BIRDS')" href="javascript:void(0)"><s:text name="for.return"></s:text>
    Category</a>
</div>

<div id="Catalog">

    <h2 id="product-name">Product Name</h2>

    <table id="item-list">
        <tr>
            <th><s:text name="for.itemid"></s:text></th>
            <th><s:text name="for.proid"></s:text></th>
            <th><s:text name="for.des"></s:text></th>
            <th><s:text name="for.price"></s:text></th>
            <th>&nbsp;</th>
        </tr>
        <!-- list -->
    </table>
</div>
<script type="application/javascript">
    $(document).ready(function () {
        getItem();
    })
</script>
    <%@ include file="../common/IncludeBottom.jsp" %>