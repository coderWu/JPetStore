<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/17
  Time: 18:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>

<div id="BackLink">
    <a href="CatalogPage_main"><s:text name="for.returntomain"></s:text></a>
</div>

<div id="Catalog">

    <table id="search-result">
        <tr>
            <th>&nbsp;</th>
            <th><s:text name="for.proid"></s:text></th>
            <th><s:text name="for.proname"></s:text></th>
        </tr>

    </table>

</div>

<script type="application/javascript">
    $(document).ready(function () {
        getSearchProduct();
    });
</script>

<%@ include file="../common/IncludeBottom.jsp" %>