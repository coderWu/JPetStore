<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/13
  Time: 21:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <link rel="StyleSheet" href="css/jpetstore.css" type="text/css"
          media="screen" />

    <meta name="generator"
          content="HTML Tidy for Linux/x86 (vers 1st November 2002), see www.w3.org" />
    <title>JPetStore Demo</title>
    <meta content="text/html; charset=windows-1252"
          http-equiv="Content-Type" />
    <meta http-equiv="Cache-Control" content="max-age=0" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <script src="https://cdn.bootcss.com/jquery/3.2.0/jquery.js"></script>
    <link href="https://cdn.bootcss.com/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jqueryui/1.12.1/jquery-ui.js"></script>
    <script src="js/account.js"></script>
    <script src="js/catalog.js"></script>
    <script src="js/cart.js"></script>
</head>

<body>

<div id="Header">

    <div id="Logo">
        <div id="LogoContent">
            <a href="CatalogPage_main"><s:text name="for.image_logo"></s:text></a>
        </div>
    </div>

    <div id="Menu">
        <div id="MenuContent">
            <a href="CartPage_cart"><img align="middle" name="img_cart"
                                    src="images/cart.gif" /></a> <img align="middle"
                                                                         src="images/separator.gif" />
            <span id="logout-elements"><a href="AccountPage_signInPage"><s:text name="for.SignIn"></s:text></a></span>
                <span id="login-elements"><a href="AccountPage_myAccountPage">欢迎:<span id="login-user-name"></span></a>
                <a href="" id="logoutClick"><s:text name="for.Signout"></s:text></a>
                <img align="middle" src="images/separator.gif" /> <a
                    href="AccountPage_myAccountPage"><s:text name="for.MyAccount"></s:text></a></span>
             <img align="middle"
                                             src="images/separator.gif" /> <a href="help">?</a>
            <span id="admin-header" style="display: none">
                <img align="middle" src="images/separator.gif" /> <a href="CatalogPage_editItemQuantity"><s:text name="for.editItemQuantity"></s:text></a>
                <img align="middle" src="images/separator.gif" /> <a href="CatalogPage_viewedItems"><s:text name="for.viewedItems"></s:text></a>
            </span>
        </div>
    </div>

    <div id="Search">
        <div id="SearchContent">
            <%--<form>--%>
                <input type="text" id="search-name" name="keyword" size="14" />
                <input type="submit" id="search-button" name="searchProducts" value="<s:text name="for.search"/>" />
            <%--</form>--%>
        </div>
    </div>

    <div id="QuickLinks">
        <a onclick="javascript:viewCategory('FISH')" href="javascript:void(0)"><s:text name="for.image_sfish"></s:text></a> <img src="images/separator.gif" />
        <a onclick="javascript:viewCategory('DOGS')" href="javascript:void(0)"><s:text name="for.image_sdog"></s:text></a> <img src="images/separator.gif" />
        <a onclick="javascript:viewCategory('REPTILES')" href="javascript:void(0)"><s:text name="for.image_sreptiles"></s:text></a> <img src="images/separator.gif" />
        <a onclick="javascript:viewCategory('CATS')" href="javascript:void(0)"><s:text name="for.image_scat"></s:text></a> <img src="images/separator.gif" />
        <a onclick="javascript:viewCategory('BIRDS')" href="javascript:void(0)"><s:text name="for.image_sbird"></s:text></a>
    </div>

</div>
<script type="application/javascript">
    var logout = $("#logout-elements");
    var login = $("#login-elements");
    var accountLogout = true;
    $(document).ready(function () {
        //var loginUser = getLoginUserInfo();
        getLoginUserInfo();
        if (loginUser == 400) {
            accountLogout = true;
        } else if (loginUser.code != 200) {
            accountLogout = true;
        } else {
            //菜单栏显示登陆还是登出
            var info = loginUser.data;
            $("#login-user-name").text(info.username);
            accountLogout = false;
            //设置下面banner的值
            setBanner();
        }
        if (!accountLogout) {
            login.show();
            logout.hide();
        } else {
            login.hide();
            logout.show();
        }
    });
    $("#logoutClick").click(function () {
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/_Account_logout",
            type : "POST",
            success : function (data) {
                if (data.code == 200) {
                    $("#logout").show();
                    $("#login").hide();
                } else {
                    alert("登出失败");
                }

            }
        });
    });
    $("#search-name").keyup(function () {
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/_Catalog_searchRemind",
            type : "POST",
            data : { searchProducts : $("#search-name").val() },
            dataType : "json",
            success : function (data) {
                var proposals = [];
                var count = 0;
                $.each(data.data, function (k, v) {
                    proposals[count] = v.name;
                    count++;
                });
                $("#search-name").autocomplete({
                    source : proposals
                });
            }
        });
    });

    $("#search-button").click(function () {
        searchProduct($("#search-name").val());
    });
</script>
<div id="Content">
