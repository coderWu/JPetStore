<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/21
  Time: 20:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="../common/IncludeTop.jsp" %>
<div id="BackLink">
    <a href="CatalogPage_main">Return to Main Menu</a>
</div>

<div id="Catalog">
    Please confirm the information below and then press continue...

    <table>
        <tbody>
        <tr>
            <th align="center" colspan="2"><font size="4"><b>Order</b></font>
                <br> <font size="3"><b id="cOrderDate"></b></font></th>
        </tr>

        <tr>
            <th colspan="2">Billing Address</th>
        </tr>
        <tr>
            <td>First name:</td>
            <td id="cBillToFirstName"></td>
        </tr>
        <tr>
            <td>Last name:</td>
            <td id="cBillToLastName"></td>
        </tr>
        <tr>
            <td>Address 1:</td>
            <td id="cBillAddress1"></td>
        </tr>
        <tr>
            <td>Address 2:</td>
            <td id="cBillAddress2"></td>
        </tr>
        <tr>
            <td>City:</td>
            <td id="cBillCity"></td>
        </tr>
        <tr>
            <td>State:</td>
            <td id="cBillState"></td>
        </tr>
        <tr>
            <td>Zip:</td>
            <td id="cBillZip"></td>
        </tr>
        <tr>
            <td>Country:</td>
            <td id="cBillCountry"></td>
        </tr>
        <tr>
            <th colspan="2">Shipping Address</th>
        </tr>
        <tr>
            <td>First name:</td>
            <td id="cShipToFirstName"></td>
        </tr>
        <tr>
            <td>Last name:</td>
            <td id="cShipToLastName"></td>
        </tr>
        <tr>
            <td>Address 1:</td>
            <td id="cShipAddress1"></td>
        </tr>
        <tr>
            <td>Address 2:</td>
            <td id="cShipAddress2"></td>
        </tr>
        <tr>
            <td>City:</td>
            <td id="cShipCity"></td>
        </tr>
        <tr>
            <td>State:</td>
            <td id="cShipState"></td>
        </tr>
        <tr>
            <td>Zip:</td>
            <td id="cShipZip"></td>
        </tr>
        <tr>
            <td>Country:</td>
            <td id="cShipCountry"></td>
        </tr>

        </tbody>
    </table>

    <br><span><br><a style="display: none;color: red;" id="error-message"></a></span>
    <a class="Button" id="confirm_order">Confirm</a>
</div>
<script type="application/javascript">
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/_Order_getOrder",
        type : "POST",
        success : function (data) {
            if (data.code == 200) {
                console.log(data);
                var info = data.data;
                $("#cBillToOrderDate").text(info.orderDate);
                $("#cBillToFirstName").text(info.billToFirstName);
                $("#cBillToLastName").text(info.billToLastName);
                $("#cBillAddress1").text(info.billAddress1);
                $("#cBillAddress2").text(info.billAddress2);
                $("#cBillCity").text(info.billCity);
                $("#cBillCountry").text(info.billCountry);
                $("#cBillState").text(info.billState);
                $("#cBillZip").text(info.billZip);
                $("#cShipToFirstName").text(info.shipToFirstName);
                $("#cShipToLastName").text(info.shipToLastName);
                $("#cShipAddress1").text(info.shipAddress1);
                $("#cShipAddress2").text(info.shipAddress2);
                $("#cShipCity").text(info.shipCity);
                $("#cShipState").text(info.shipState);
                $("#cShipZip").text(info.shipZip);
                $("#cShipCountry").text(info.shipCountry);
            } else {
                $("#error-message").text("订单不存在").show();
            }

        }
    });
    $("#confirm_order").click(function () {
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/_Order_confirm",
            type : "POST",
            data : {confirm : true},
            success : function (data) {
                if (data.code == 200) {
                    window.location.href = "//127.0.0.1:8080/JPetStore/Order_viewOrder";
                }
            }
        })
    });
</script>


<%@ include file="../common/IncludeBottom.jsp" %>
