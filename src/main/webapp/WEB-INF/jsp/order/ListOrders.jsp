<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/21
  Time: 20:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>
<h2>My Orders</h2>

<table id="orderList">
    <tr>
        <th>Order ID</th>
        <th>Date</th>
        <th>Total Price</th>
    </tr>
    <!-- 循环 -->

</table>
<br><a style="display: none;color: red;" id="error-message"></a>
<script type="application/javascript">
    $(document).ready(function () {
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/_Order_getMyOrders",
            type : "POST",
            success : function (data) {
                console.log(data);
                var content = "";
                $.each(data.data, function (k, v) {
                    content += "<tr><td><a onclick='javascript:viewOrder(" + '"' + v.orderId +
                        '"' + ")' href='javascript:void(0)'>" + v.orderId + "</a></td>" +
                        "<td>" + v.orderDate + "</td><td>$" + v.totalPrice + "</td></tr>";
                });
                $("#orderList").append(content);
            },
            error : function (data) {
                $("#error-message").text(data).show();
            }
        })
    });

    function viewOrder(orderId) {
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/Order_viewOrder",
            type : "POST",
            data : { viewOrderId : orderId },
            dataType : "json",
            success : function (data) {}
        });
        window.location.href = "//127.0.0.1:8080/JPetStore/Order_viewOrder";
    }
</script>

<%@ include file="../common/IncludeBottom.jsp" %>