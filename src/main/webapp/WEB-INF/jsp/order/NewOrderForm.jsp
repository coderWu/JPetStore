<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/21
  Time: 20:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="../common/IncludeTop.jsp" %>
<div id="Catalog">

        <table id="newOrder">
            <tbody><tr>
                <th colspan="2">Payment Details</th>
            </tr>
            <tr>
                <td>Card Type:</td>
                <td><select id="newCardType" name="cardType">
                    <option selected="selected" value="Visa">Visa</option><option value="MasterCard">MasterCard</option><option value="American Express">American Express</option>
                </select></td>
            </tr>
            <tr>
                <td>Card Number:</td>
                <td><input class="payment" id="newCreditCard" name="creditCard" value="" type="text"> * Use a fake
                    number!</td>
            </tr>
            <tr>
                <td>Expiry Date (MM/YYYY):</td>
                <td><input class="payment" id="newExpiryDate" name="expiryDate" value="" type="text"></td>
            </tr>
            <tr>
                <th colspan="2">Billing Address</th>
            </tr>

            <tr>
                <td>First name:</td>
                <td><input id="newBillToFirstName" name="billToFirstName" value="" type="text"></td>
            </tr>
            <tr>
                <td>Last name:</td>
                <td><input id="newBillToLastName" name="billToLastName" value="" type="text"></td>
            </tr>
            <tr>
                <td>Address 1:</td>
                <td><input id="newBillAddress1" name="billAddress1" value="" type="text" size="40"></td>
            </tr>
            <tr>
                <td>Address 2:</td>
                <td><input id="newBillAddress2" name="billAddress2" value="" type="text" size="40"></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><input id="newBillCity" name="billCity" value="" type="text"></td>
            </tr>
            <tr>
                <td>State:</td>
                <td><input id="newBillState" name="billState" value="" type="text" size="4"></td>
            </tr>
            <tr>
                <td>Zip:</td>
                <td><input id="newBillZip" name="billZip" value="" type="text" size="10"></td>
            </tr>
            <tr>
                <td>Country:</td>
                <td><input id="newBillCountry" name="billCountry" value="" type="text" size="15"></td>
            </tr>

            <tr>
                <td colspan="2"><input name="shippingAddressRequired" id="isAddReq" type="checkbox">
                    Ship to different address...</td>
            </tr>

            </tbody></table>
        <br><span><br><a style="display: none;color: red;" id="error-message"></a></span>
        <input name="newOrder" id="newOrderSubmit" value="Continue" type="submit">

</div>

<script type="application/javascript">

    $(document).ready(function () {
        var information = loginUser.data;
        $("#newBillToFirstName").val(information.firstName);
        $("#newBillToLastName").val(information.lastName);
        $("#newBillAddress1").val(information.address1);
        $("#newBillAddress2").val(information.address2);
        $("#newBillCity").val(information.city);
        $("#newBillState").val(information.state);
        $("#newBillZip").val(information.zip);
        $("#newBillCountry").val(information.country);
    });

    function checkOrderForm() {
        var noNone = 0;
        $("#newOrder input").each(function () {
            var value = $(this).val();
            if (value == "") {
                noNone++;
                removeErrorMessage($(this));
                $(this).after(showErrorMessage("请填写该项"));
            } else {
                //如果后面有之前残留的错误提示则清空
                removeErrorMessage($(this));
            }
        });
        return noNone;
    }

    function getOrderData() {
        var formData = new FormData();
        $("#newOrder input").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            if (e_name != "newOrder") {
                formData.append(e_name, value);
            }
        });
        $("#newOrder select").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            formData.append(e_name, value);
        });
        return formData;
    }

    $("#newOrderSubmit").click(function () {
        var data = getOrderData();
        console.log(data);
        console.log( $("#isAddReq").is(":checked"));
        data.append("different", $("#isAddReq").is(":checked"));
        if (checkOrderForm() === 0) {
            $.ajax({
                url : "//127.0.0.1:8080/JPetStore/_Order_editOrder",
                data : data,
                type : "POST",
                dataType : "json",
                cache: false,
                processData: false,
                contentType: false,
                success : function (data) {
                    if (data.code == 200) {
                        if ($("#isAddReq").is(":checked")) {
                            window.location.href = "//127.0.0.1:8080/JPetStore/Order_shipForm";
                        } else {
                            window.location.href = "//127.0.0.1:8080/JPetStore/Order_confirmOrder";
                        }
                    } else {
                        $("#error-message").text("提交失败").show();
                    }
                }
            });
        }
    });

</script>

<%@ include file="../common/IncludeBottom.jsp" %>