
<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/21
  Time: 20:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="../common/IncludeTop.jsp" %>
<div id="Catalog">

    <table id="newShip">
        <tr>
            <th colspan=2>Shipping Address</th>
        </tr>

        <tr>
            <td>First name:</td>
            <td><input id="shipFirstName" name="shipToFirstName" /></td>
        </tr>
        <tr>
            <td>Last name:</td>
            <td><input id="shipLastName" name="shipToLastName" /></td>
        </tr>
        <tr>
            <td>Address 1:</td>
            <td><input id="shipAddress1" size="40" name="shipAddress1" /></td>
        </tr>
        <tr>
            <td>Address 2:</td>
            <td><input id="shipAddress2" size="40" name="shipAddress2" /></td>
        </tr>
        <tr>
            <td>City:</td>
            <td><input id="shipCity" name="shipCity" /></td>
        </tr>
        <tr>
            <td>State:</td>
            <td><input id="shipState" size="4" name="shipState" /></td>
        </tr>
        <tr>
            <td>Zip:</td>
            <td><input id="shipZip" size="10" name="shipZip" /></td>
        </tr>
        <tr>
            <td>Country:</td>
            <td><input id="shipCountry" size="15" name="shipCountry" /></td>
        </tr>


    </table>

    <input type="submit" id="newShipSubmit" name="newOrder" value="Continue" />

</div>

<script type="application/javascript">
    function checkShipForm() {
        var noNone = 0;
        $("#newShip input").each(function () {
            var value = $(this).val();
            if (value == "") {
                noNone++;
                removeErrorMessage($(this));
                $(this).after(showErrorMessage("请填写该项"));
            } else {
                //如果后面有之前残留的错误提示则清空
                removeErrorMessage($(this));
            }
        });
        return noNone;
    }

    function getShipData() {
        var formData = new FormData();
        $("#newShip input").each(function () {
            var value = $(this).val();
            var e_name = $(this).attr('name');
            if (e_name != "newOrder") {
                formData.append(e_name, value);
            }
        });
        return formData;
    }

    $("#newShipSubmit").click(function () {
        var data = getShipData();

        if (checkShipForm() === 0) {
            $.ajax({
                url : "//127.0.0.1:8080/JPetStore/_Order_editShip",
                data : data,
                type : "POST",
                dataType : "json",
                cache: false,
                processData: false,
                contentType: false,
                success : function (data) {
                    console.log(data);
                    if (data.code == 200) {
                        window.location.href = "//127.0.0.1:8080/JPetStore/Order_confirmOrder";
                    } else {
                        $("#error-message").text("提交失败").show();
                    }
                }
            });
        }
    });
</script>

<%@ include file="../common/IncludeBottom.jsp" %>
