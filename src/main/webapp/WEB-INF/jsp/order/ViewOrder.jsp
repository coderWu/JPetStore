<%--
  Created by IntelliJ IDEA.
  User: Jehu
  Date: 2017/3/21
  Time: 20:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../common/IncludeTop.jsp" %>

<ul class="messages">
    <li>Thank you, your order has been submitted.</li>
</ul>
<div id="BackLink">
    <a href="../catalog/Main.html">Return to Main Menu</a>
</div>

<div id="Catalog">

    <table>
        <tbody>
        <tr>
            <th align="center" colspan="2" id="vHead">Order #</th>
        </tr>
        <tr>
            <th colspan="2">Payment Details</th>
        </tr>
        <tr>
            <td>Card Type:</td>
            <td id="vCardType">MasterCard</td>
        </tr>
        <tr>
            <td>Card Number:</td>
            <td id="vCardNum">999 9999 9999 9999 * Fake number!</td>
        </tr>
        <tr>
            <td>Expiry Date (MM/YYYY):</td>
            <td id="vExpiryDate">12/03</td>
        </tr>
        <tr>
            <th colspan="2">Billing Address</th>
        </tr>
        <tr>
            <td>First name:</td>
            <td id="vBillToFirstName"></td>
        </tr>
        <tr>
            <td>Last name:</td>
            <td id="vBillToLastName"></td>
        </tr>
        <tr>
            <td>Address 1:</td>
            <td id="vBillAddress1"></td>
        </tr>
        <tr>
            <td>Address 2:</td>
            <td id="vBillAddress2"></td>
        </tr>
        <tr>
            <td>City:</td>
            <td id="vBillCity"></td>
        </tr>
        <tr>
            <td>State:</td>
            <td id="vBillState"></td>
        </tr>
        <tr>
            <td>Zip:</td>
            <td id="vBillZip"></td>
        </tr>
        <tr>
            <td>Country:</td>
            <td id="vBillCountry"></td>
        </tr>
        <tr>
            <th colspan="2">Shipping Address</th>
        </tr>
        <tr>
            <td>First name:</td>
            <td id="vShipToFirstName"></td>
        </tr>
        <tr>
            <td>Last name:</td>
            <td id="vShipToLastName"></td>
        </tr>
        <tr>
            <td>Address 1:</td>
            <td id="vShipAddress1"></td>
        </tr>
        <tr>
            <td>Address 2:</td>
            <td id="vShipAddress2"></td>
        </tr>
        <tr>
            <td>City:</td>
            <td id="vShipCity"></td>
        </tr>
        <tr>
            <td>State:</td>
            <td id="vShipState"></td>
        </tr>
        <tr>
            <td>Zip:</td>
            <td id="vShipZip"></td>
        </tr>
        <tr>
            <td>Country:</td>
            <td id="vShipCountry"></td>
        </tr>
        <tr>
            <td>Courier:</td>
            <td id="vCourier"></td>
        </tr>
        <tr>
            <td colspan="2" id="vStatus">Status: </td>
        </tr>
        <tr>
            <td colspan="2">
                <table>
                    <tbody id="orderItem">
                    <tr>
                        <th>Item ID</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total Cost</th>
                    </tr>


                    </tbody>
                </table>
            </td>
        </tr>

        </tbody>
    </table>

</div>

<script type="application/javascript">
    $(document).ready(function () {
        $.ajax({
            url : "//127.0.0.1:8080/JPetStore/_Order_getViewOrder",
            type : "POST",
            success : function (data) {
                if (data.code == 200) {
                    console.log(data);
                    var info = data.data;
                    $("#vHead").append(info.orderId + " " + info.orderDate);
                    $("#vCardType").text(info.cardType);
                    $("#vCardNum").text(info.creditCard);
                    $("#vExpiryDate").text(info.expiryDate);
                    $("#vBillToFirstName").text(info.billToFirstName);
                    $("#vBillToLastName").text(info.billToLastName);
                    $("#vBillAddress1").text(info.billAddress1);
                    $("#vBillAddress2").text(info.billAddress2);
                    $("#vBillCity").text(info.billCity);
                    $("#vBillCountry").text(info.billCountry);
                    $("#vBillState").text(info.billState);
                    $("#vBillZip").text(info.billZip);
                    $("#vShipToFirstName").text(info.shipToFirstName);
                    $("#vShipToLastName").text(info.shipToLastName);
                    $("#vShipAddress1").text(info.shipAddress1);
                    $("#vShipAddress2").text(info.shipAddress2);
                    $("#vShipCity").text(info.shipCity);
                    $("#vShipState").text(info.shipState);
                    $("#vShipZip").text(info.shipZip);
                    $("#vShipCountry").text(info.shipCountry);
                    $("#vCourier").text(info.courier);
                    $("#vStatus").append(info.status);
                    var content = "";
                    $.each(info.lineItems, function (k, v) {
                        var description = v.item.attribute1 == null ? "" : v.item.attribute1;
                        description += v.item.attribute2 == null ? "" : " " + v.item.attribute2;
                        description += v.item.attribute3 == null ? "" : " " + v.item.attribute3;
                        description += v.item.attribute4 == null ? "" : " " + v.item.attribute4;
                        description += v.item.attribute5 == null ? "" : " " + v.item.attribute5;
                        description += " ";
                        content += "<tr><td><a onclick='javascript:viewItem(" + '"' + v.itemId +
                            '"' + ")' href='javascript:void(0)'>" + v.itemId + "</a></td><td>" + description +
                            "</td><td>" + v.quantity + "</td><td>$" + v.unitPrice + "</td>" +
                            "<td>$" + v.total + "</td></tr>";
                    });
                    content += "<tr><th colspan='5'>Total: $" + info.totalPrice + "</th> </tr>";
                    $("#orderItem").append(content);
                } else {
                    $("#error-message").text("订单不存在").show();
                }

            }
        });
    });
</script>

<%@ include file="../common/IncludeBottom.jsp" %>