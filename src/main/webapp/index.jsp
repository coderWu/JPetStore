<%--
  Created by IntelliJ IDEA.
  User: zhangzhenghao
  Date: 17/4/26
  Time: 下午9:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
    <title>JPetStore Demo</title>
    <link rel="StyleSheet" href="css/jpetstore.css" type="text/css" media="screen" />
</head>

<body>
<div id="Content">
    <h2><s:text name="for.welcome"></s:text></h2>
    <p>
        <a href="CatalogPage_main"><s:text name="for.enter"></s:text></a>
    </p>
    <p>
        <sub><s:text name="for.copyright"></s:text></sub>
    </p>
</div>
</body>
