/**
 *
 * Created by Jehu on 2017/3/16.
 */

function removeErrorMessage(_obj) {
    var next = _obj.next("span");
    if (typeof (next[0]) != "undefined") {
        next.remove();
        removeErrorMessage(_obj);
    }
}

function showErrorMessage(data) {
    return "<span style='color: red'>" + data + "</span>";
}
var loginUser;//全局保存
function getLoginUserInfo() {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/_Account_getUser",
        type : "POST",
        data : {},
        dataType : "json",
        async : false,
        success : function (data) {
            loginUser = data;
            console.log(loginUser);
            if(loginUser.data.admin == "1"){
                $("#admin-header").show();
            }
        },
        error : function () {
            loginUser = 400;
        }
    });
}
