/**
 * Created by Jehu on 2017/3/23.
 */
function addToCart(itemId) {
    updateCart(itemId, 1);
    window.location.href = "//127.0.0.1:8080/JPetStore/CartPage_cart";
}

function updateCart(itemId, count) {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/_Cart_addItem",
        type : "POST",
        data : {itemId : itemId, count : count},
        dataType : "json",
        success : function (data) {}
    });
}


function getCart(isSummary) {
    var message = $("#cart-message");
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/_Cart_getCartData",
        type : "POST",
        success : function (data) {
            if (data.code == 200) {
                var cartItem = data.data;
                if (cartItem.numberOfItems > 0) {
                    var content = editCartContent(cartItem, isSummary);
                    $("#cart-list").append(content);
                } else {
                    message.find('b').text("购物车是空的");
                    message.show();
                    $("#check-out-button").hide();
                    $("#check-out").hide();
                }
            } else {
                message.find('b').text(data.data);
                message.show();
                $("#check-out-button").hide();
                $("#check-out").hide();
            }
        },
        error : function () {
            message.find('b').text("获取购物车信息失败");
            message.show();
            $("#check-out-button").hide();
            $("#check-out").hide();
        }
    });
}

function editCartContent(cartItem, isSummary) {
    var content = "";
    $.each(cartItem.cartItemList, function (k, v) {
        var item = v.item;
        var description = item.attribute1 == null ? "" : item.attribute1;
        description += item.attribute2 == null ? "" : " " + item.attribute2;
        description += item.attribute3 == null ? "" : " " + item.attribute3;
        description += item.attribute4 == null ? "" : " " + item.attribute4;
        description += item.attribute5 == null ? "" : " " + item.attribute5;
        description += " ";
        description += item.product.name;
        //TODO 修改删除链接
        content += "<tr id='" + item.itemId + "'><td><a onclick='javascript:viewItem(" + '"' +  item.itemId +
            '"' + ")' href='javascript:void(0)'>" + item.itemId + "</a></td><td>" +
            item.product.productId + "</td><td>" + description + "</td><td>" + v.inStock +
            "</td><td>";
        if (!isSummary) {
            content += "<input type='text' size='3' class='cart-item' name='" + item.itemId + "' value='"
                + v.quantity + "' onblur='javascript:isStockEnough(this," + '"' +  item.itemId + '"' + ")'" + "/>";        } else {
            content += v.quantity;
        }
        content += "</td><td>" + item.listPrice + "</td><td>" + v.total + "</td>";
        if (!isSummary) {
            content += "<td><a onclick='javascript:removeItem(" + '"' +  item.itemId +
                '"' + ")' href='javascript:void(0)' class='Button'>Remove</a></td>"
        }
        content += "</tr>";
    });
    content += "<tr><td colspan='7'>Sub Total:" + cartItem.subTotal;
    if (!isSummary) {
        content += "<input class='Button' type='submit' name='submit' " +
            "onclick='javascript:updateCartAction()' value='Update Cart'/></td><td>&nbsp;</td></tr>";
    }

    return content;
}

function isStockEnough(obj, itemId) {
    //var quantity = obj.attributes.value.value;
    var message = $("#cart-message");
    var quantity = obj.value;
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/_Catalog_isStockEnough",
        type : "post",
        data :{
            itemId: itemId,
            quantity: quantity
        },
        success : function (data) {
            if (data.code == 200){
                if (data.data == false){
                    alert("low stocks");
                }
            }
        },
        error: function () {
            message.find('b').text("Failed to get data");
            message.show();
        }
    });
}