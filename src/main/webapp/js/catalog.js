/**
 * Created by Jehu on 2017/3/18.
 */

function setBanner() {
    $("#Banner").append(loginUser.data.bannerName.replace(/\.\.\// ,''));
}

function viewCategory(category) {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/CatalogPage_category",
        type : "POST",
        data : { category : category },
        dataType : "json",
        success : function (data) {}
    });
    window.location.href = "//127.0.0.1:8080/JPetStore/CatalogPage_category";
}

function viewProduct(product) {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/CatalogPage_product",
        type : "POST",
        data : { product : product },
        dataType : "json",
        success : function (data) {}
    });
    window.location.href = "//127.0.0.1:8080/JPetStore/CatalogPage_product";
}

function viewItem(item) {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/CatalogPage_item",
        type : "POST",
        data : { item : item },
        dataType : "json",
        success : function (data) {}
    });
    window.location.href = "//127.0.0.1:8080/JPetStore/CatalogPage_item";
}

function searchProduct(searchName) {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/CatalogPage_searchProducts",
        type : "POST",
        data : { searchProducts : searchName },
        dataType : "json",
        success : function (data) {}
    });
    window.location.href = "//127.0.0.1:8080/JPetStore/CatalogPage_searchProducts";
}


function getProduct() {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/_Catalog_getCategoryData",
        type : "POST",
        data : {},
        dataType : "json",
        success : function (data) {
            if (data.code == 200) {
                $.each(data.data, function (k, v) {
                    $("#category-name").text(v.category);
                    $("#product-list").append(
                    "<tr><td><a onclick='javascript:viewProduct(" + '"' +  v.productId +
                    '"' + ")' href='javascript:void(0)'>" + v.productId + "</a></td>" +
                    "<td>" + v.name + "</td></tr>"
                    );
                });
            }
        }
    });
}

function getItem() {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/_Catalog_getProductData",
        type : "POST",
        data : {},
        dataType : "json",
        success : function (data) {
            if (data.code == 200) {
                $("#product-name").text(data.data[0].name);
                var back_link = $("#return-to-category");
                back_link.attr("onclick", "javascript:viewCategory('" + data.data[0].categoryId + "')");
                back_link.attr("href", "javascript:void(0)");
                back_link.text("Return to " + data.data[0].categoryId);
                $.each(data.data[1], function (k, v) {
                    //TODO 修改添加购物车链接
                    var description = v.attribute1 == null ? "" : v.attribute1;
                    description += v.attribute2 == null ? "" : " " + v.attribute2;
                    description += v.attribute3 == null ? "" : " " + v.attribute3;
                    description += v.attribute4 == null ? "" : " " + v.attribute4;
                    description += v.attribute5 == null ? "" : " " + v.attribute5;
                    description += " ";
                    $("#item-list").append("<tr><td><a onclick='javascript:viewItem(" + '"' + v.itemId +
                        '"' + ")' href='javascript:void(0)'>" + v.itemId + "</a></td>" + "<td>"+ v.product.productId +
                        "</td><td>"+ description + data.data[0].name + "</td><td>$" + v.listPrice + "</td><td>" +
                        "<a onclick='javascript:addToCart(" + '"' + v.itemId +
                        '"' + ")' href='javascript:void(0)' class='Button'>Add to Cart</a>" + "</td>" + "</tr>");
                });
            }
        }
    });
}

function getItemDetail() {
    $.ajax({
        url : "//127.0.0.1:8080/JPetStore/_Catalog_getItemData",
        type : "POST",
        data : {},
        dataType : "json",
        success : function (data) {
            console.log(data);
            if (data.code == 200) {
                var item = data.data;
                var back_link = $("#return-to-product");
                back_link.attr("onclick", "javascript:viewProduct('" + item.product.productId + "')");
                back_link.attr("href", "javascript:void(0)");
                back_link.text("Return to " + item.product.productId);
                var description = item.attribute1 == null ? "" : item.attribute1;
                description += item.attribute2 == null ? "" : " " + item.attribute2;
                description += item.attribute3 == null ? "" : " " + item.attribute3;
                description += item.attribute4 == null ? "" : " " + item.attribute4;
                description += item.attribute5 == null ? "" : " " + item.attribute5;
                description += " ";
                //TODO url处理
                item.product.description = item.product.description.replace(/\.\.\// ,'');
                console.log(item.product.description);
                var addContent = "<tr><td>" + item.product.description + "</td></tr>" +
                        "<tr><td><b>" + item.itemId + "</b></td></tr>" +
                        "<tr><td><b><font size='4'>" + description + item.product.name +
                        "</font></b></td></tr>" +
                        "<tr><td>" + item.product.name + "</td></tr>" +
                        "<tr><td>" + item.quantity + " in stock." + "</td></tr>" +
                        "<tr><td>$" + item.listPrice + "</td></tr>" +
                        "<tr><td><a class='Button' onclick='javascript:addToCart(" + '"' + item.itemId +
                    '"' + ")' href='javascript:void(0)'>Add to Cart</a></td></tr>";
                $("#item-detail").append(addContent);
            }
        }
    });
}

function getSearchProduct() {
    $.ajax({
        url: "//127.0.0.1:8080/JPetStore/_Catalog_searchProduct",
        type: "POST",
        data: {},
        dataType: "json",
        success: function (data) {
            if (data.code == 200) {
                $.each(data.data, function (k, v) {
                    v.description = v.description.replace(/\.\.\// ,'');
                    var addContent = "<tr><td><a onclick='javascript:viewProduct(" + '"' +  v.productId +
                        '"' + ")' href='javascript:void(0)'>" + v.description + "</a></td><td><b><a onclick='javascript:viewProduct(" +
                        '"' +  v.productId + '"' + ")' href='javascript:void(0)'><font color='BLACK'>" +
                        v.productId + "</font></a></b></td><td>" + v.name + "</td></tr>";
                    $("#search-result").append(addContent);
                });
            } else {
                $("#search-result").text(data.data);
            }
        },
        error : function () {
            $("#search-result").text("啊偶，出错了");
        }
    });
}

/**
 * tujie
 * 得到所有的item，admin可以修改quantity
 * 即修改库存
 */
function getItemList() {
    $.ajax({
        url: "//127.0.0.1:8080/JPetStore/_Catalog_getItemList",
        type: "POST",
        data: {},
        dataType: "json",
        success: function (data) {
            if (data.code == 200){
                var itemListData = data.data;
                var content = getItemContent(itemListData);
                $("#item-list").append(content);
            }
        },
        error: function () {
            $("#item-list").append("<tr><td colspan='6'>Failed to get data</td></tr>");
        }
    });
}

/**
 * 将ItemList的数据以表格的方式显示出来
 * @param itemListData
 * @returns {string}
 */
function getItemContent(itemListData) {
    var content = "";
    $.each(itemListData, function (index, item) {
        content += "<tr><td>" +
            "<a onclick='javascript:viewItem(" + '"' +  item.itemId + '"' + ")' href='javascript:void(0)'>" + item.itemId + "</a></td><td>" +
            item.productId + "</td><td>" + item.attribute1 + "</td><td>" +
            "<input type='text' size='3' class='cart-item' name='" + item.itemId + "' value='" + item.quantity + "' id='"+ item.itemId +"'/>" +
            "<td>"+ item.listPrice +"</td>"
            + "<td><a onclick='javascript:updataItemQuantity(" + '"' +  item.itemId + '"' + ")' href='javascript:void(0)' class='Button'>update</a></td>"
            + "</tr>";
    });
    return content;
}

/**
 * 更新item的quantity，相当于更新库存inventory中的qty
 * @param itemId
 */
function updataItemQuantity(itemId) {
    var quantity = document.getElementById(itemId).value;
    $.ajax({
        url: "//127.0.0.1:8080/JPetStore/_Catalog_updateItemQuantity",
        type: "post",
        data: {
            itemId: itemId,
            quantity :quantity
        },
        datatype: "json",
        success: function (data) {
            if (data.code == 200){
                window.alert("update successful");
            }
        }
    });
}

function getViewedItemList() {
    $.ajax({
        url: "//127.0.0.1:8080/JPetStore/_Catalog_getViewedItemList",
        type: "POST",
        data: {},
        dataType: "json",
        success: function (data) {
            if (data.code == 200){
                var viewedItemListData = data.data;
                var content = getViewedItemContent(viewedItemListData);
                $("#viewed-item-list").append(content);
            }
        },
        error: function () {
            $("#viewed-item-list").append("<tr><td colspan='7'>Failed to get data</td></tr>");
        }
    });
}

function getViewedItemContent(viewedItemListData) {
    var content = "";
    $.each(viewedItemListData, function (index, item) {
        content += "<tr><td>" + item.username + "</td><td>" +
            "<a onclick='javascript:viewItem(" + '"' +  item.itemId + '"' + ")' href='javascript:void(0)'>" + item.itemId + "</a></td><td>" +
            item.productId + "</td><td>" + item.attribute1 + "</td><td>" +
            item.listPrice +"</td><td>" + item.datetime + "</td>"
            + "</tr>";
    });
    return content;
}
